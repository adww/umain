---
title: Scrum Master JR
layout: post
date: '2021-05-01 16:07:44 -0300'
categories:
- job
tag:
- Scrum
- Processos Ágeis
logo: img/empresa/pepsi.png
contratacao: remoto
setor: Agilidade
empresa: Pepsi
---

# Descrição
Estamos procurando um Scrum Master JR para atuar junto aos times de um Projeto de Desenvolvimento de Software, liderando a abordagem Scrum, facilitando cerimônias e maximizando o engajamento dos membros do Projeto.
* A pessoa selecionada, dentre outras atividades, ficará responsável por:
* Treinar equipes na metodologia Scrum
* Apoiar equipes na transição do mindset tradicional para o mindset ágil;
* Desenvolver padrões e definições para o processo ágil;
* Atuar em Projetos no apoio as equipes, remoção de impedimentos e facilitação de cerimônias.

# Requisitos
Superior Completo nas áreas de Tecnologia

Conhecimentos em: 
* Scrum
* Facilitação de Cerimônias
* Gestão Ágil de Projetos
* Processos Ágeis de Engenharia de Software

# Benefícios: 
* Plano de Saúde/Odontológico
* Vale Refeição/Alimentação
* Day Off (Bday)
* Reembolso em certificações
* Incentivo à educação
* Auxílio Home Office
* Jornada: 8h às 18h (2h de intervalo) de segunda a sexta-feira.
