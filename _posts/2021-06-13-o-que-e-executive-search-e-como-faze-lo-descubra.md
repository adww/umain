---
title: O que é executive search e como fazê-lo? Descubra!
categories:
- blog
layout: post
banner: https://www.gupy.io/hs-fs/hubfs/executive-search.jpeg
---

O executive search é uma forma de recrutar profissionais para cargos de alta gestão, de maneira ativa, discreta e eficiente. Geralmente, o processo é realizado por um headhunter, com experiência no assunto e uma boa rede de contatos.

Encontrar a pessoa certa para a vaga certa: essa é uma preocupação diária dos responsáveis pelos processos de recrutamento e seleção e frase frequente em qualquer material sobre o assunto.

Falando especificamente da captação de profissionais, a questão tem ainda mais importância quando a seleção envolve a procura de executivos para ocupar cargos de alto nível.

É nesse contexto que entra o executive search, serviço fundamental para que os resultados sejam satisfatórios, tanto na escolha do candidato certo, como no resultado do processo seletivo em si, viabilizando economia de recursos de tempo e dinheiro.

Neste post, você vai conhecer:

* conceito de executive search;
* papel do headhunter;
* modo de fazer o executive search;
* motivos para utilizar o executive search.
Acompanhe!
