---
title: Marketing Specialist
layout: post
logo: img/empresa/adeccogroup.png
empresa: Adecco Group
categories: job
setor: Marketing
---

Mit uns kommen Sie einen Schritt weiter auf der Karriereleiter

The Adecco Group ist der Weltmarktführer im Bereich Personaldienstleistungen. Werden Sie Teil unseres Teams und unseres wegweisenden, zukunftsorientierten, proaktiven Unternehmens. Wir schauen nicht nur hoffnungsvoll in die Zukunft, wir gestalten sie mit.

Ihre Aufgaben

Die eigenständige Umsetzung von Marketingprojekten für einen langjährigen Kunden der Adecco Group in Deutschland
Kreation und Umsetzung von zielgruppengerechter Marketingkommunikation für Kandidaten in allen Kanälen, on- und off-digital sowie Werbekampagnen
Mitwirkung bei der Durchführung von konjunkturellen Aufgaben inklusive Erstellung von Werbematerialien, Erstellung von Zusammenfassungen und Reports
Analyse der Wirksamkeit und Berichterstattung über die durchgeführten Marketingaktivitäten
Erstellung von Zusammenfassungen und Berichten
Pflege von Kontakten und Beziehungen während der Durchführung von Projekten
Unterstützung der Verkaufsaktivitäten einschließlich der Erstellung der entsprechenden Berichte
Mitorganisation von und Teilnahme an Veranstaltungen für Kunden und Kandidaten, z.B. Branchenkonferenzen, Jobmessen
Beratung anderer Mitarbeiter in verschiedenen Marketingfragen
Ihr Profil

Sie haben gute Erfahrungen in der Marketingabteilung und idealerweise in der professionellen B2B-Dienstleistungsbranche
Sie sind in der Lage, aus Daten, die im Rahmen von Social Media-, Website- oder Marketingprojekten generiert werden, geschäftliche Schlüsse zu ziehen;
Sie sind kreativ und können selbstständig Werbemittel für Kampagnen planen
Sie verfügen über sehr gute MS-Office-Kenntnisse und sind geübt im Umgang mit Grafikprogrammen
Sie haben die Fähigkeit, sich fließend und effizient in Deutsch und Englisch auszudrücken
Sie haben bereits Kenntnisse in integrierten Marketingtechniken aus den Bereichen: ATL, BTL, Digital und Social Media
Sie haben Erfahrung in der Erstellung und Pflege von professionellen Social Business Profilen
Sie haben die Fähigkeit, selbstständig Marketingprojekte zu planen und durchzuführen
Ihre Perspektiven

Wir sind ein dynamisches Unternehmen, in dem Teamarbeit und Beziehungsmanagement sehr wichtig sind
Sie erhalten einen Einblick in die Arbeitswelt eines internationalen Konzerns und des weltweit größten Anbieters von Personaldienstleistungen
Anspruchsvolle Aufgaben und enge Zusammenarbeit mit einem kompetenten Team
Sie können wertvolle Praxiserfahrung und interessante Kontaktmöglichkeiten sammeln
Unser Ziel: Wir wollen sicherstellen, dass Menschen weltweit inspiriert, motiviert und weitergebildet werden, um die Zukunft der Arbeit voran zu treiben. In einer Umgebung, in der Sie ermutigt werden, sich weiter zu entwickeln und erfolgreich zu sein, möchten wir allen eine Chance geben, ihre individuelle Zukunft besser und angenehmer zu gestalten.

Interessiert? Dann freuen wir uns auf Ihre Bewerbung mit Angabe Ihres frühestmöglichen Eintrittstermins und Ihrer Gehaltsvorstellung. Jetzt auf "Bewerben" klicken!
