---
title: Clinical Sales Representative
layout: post
categories:
- job
logo: img/empresa/abbvie.jpg
contratacao: presencial
setor: Saúde
empresa: AbbvieSeto
---

Job Description
About AbbVie
AbbVie’s mission is to discover and deliver innovative medicines that solve serious health issues today and address the medical challenges of tomorrow. We strive to have a remarkable impact on people’s lives across several key therapeutic areas: immunology, oncology, neuroscience, eye care, virology, women’s health and gastroenterology, in addition to products and services across its Allergan Aesthetics portfolio. For more information about AbbVie, please visit us at www.abbvie.com. Follow @abbvie on Twitter, Facebook, Instagram, YouTube and LinkedIn.

Clinical Sales Representative – VIRGINIA BEACH, VA

As a Clinical Sales Representative, you will have the opportunity to regularly call on health care professionals (primary care physicians, clinics, hospitals and pharmacies) within a specific geographic area. Day-to-day responsibilities consist of supporting the promotional efforts behind Allergan products, which includes organizing, tracking and distributing FDA regulated sample products to health care professionals. You will maintain a current and competent level of knowledge on the product line to be a reliable source of information to the health care professionals in the territory. 


Qualifications
Qualifications:

Education

Bachelors Degree is required.
Experience:  

Candidates should possess a minimum of 1 year of experience in outside business to business sales with a proven track record of success.
Ability to work both independently and in a team setting towards meeting established objectives.
Well-developed written and oral communication skills.
Applies a range of traditional and non-traditional problem-solving techniques to think through and solve issues creatively to improve performance and empresa effectiveness.
Ability to build rapport and relationships by interacting effectively with employees and external contacts (i.e. MD and office staff) at all levels, demonstrating the awareness of their needs and responding with the appropriate action.
Highly effective organizational skills to implement a variety of programs, such as speaker programs and other activities.
Computer Skills; Word, PowerPoint, Excel and Outlook.
Some overnight travel may be required.
Candidates must be able to successfully pass background, motor and drug screen investigations.
Must have a Valid Drivers License and a clean Driving Record
Preferred Skills/Qualifications

Experience in the healthcare industry involving interaction with physicians, patients, etc. 
Experience in educating or influencing targeted customers. 

Significant Work Activities
Driving a personal auto or empresa car or truck, or a powered piece of material handling equipment
Travel
Yes, 25 % of the Time
Job Type
Experienced
Schedule
Full-time
Job Level Code
IC
Equal Employment Opportunity
At AbbVie, we value bringing together individuals from diverse backgrounds to develop new and innovative solutions for patients. As an equal opportunity employer we do not discriminate on the basis of race, color, religion, national origin, age, sex (including pregnancy), physical or mental disability, medical condition, genetic information gender identity or expression, sexual orientation, marital status, protected veteran status, or any other legally protected characteristic.
Apply
Share Job
