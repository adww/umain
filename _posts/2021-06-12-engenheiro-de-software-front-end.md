---
title: Engenheiro de Software - Front End
categories:
- job
tag: UX Design, UI Design, Design Thinking
logo: img/empresa/volkswagen.jpg
layout: post
contratacao: remoto
setor: Design
empresa: Volkswagen
---

Descrição
Estamos em busca de pessoas para apoiarem nos processos de desenvolvimento e manutenção em nossa fábrica de software, com atuação em desenvolvimento Front-end.



A pessoa selecionada realizará diversas atividades, dentre elas:



- Analisar e estimar requisitos de desenvolvimento;

- Atuar no desenvolvimento front-end, com foco na experiência do usuário;

- Atuar em projeto de reformulação de sistema desktop para web;

- Atuar em conjunto com o time de testes e configuração para liberação de produtos e atualização de revisões ou versões



Requisitos
- Graduação em andamento ou concluída em Sistema de Informação ou áreas afins;

- Experiência em desenvolvimento de software utilizando HTML, CSS e Angular.



Benefícios oferecidos: 

- Plano de Saúde e Odontológico;

- Vale Refeição/Alimentação;

- Auxílio Creche;

- Reembolso em certificações;

- Incentivo a educação;

- Seguro de vida

- Ginástica Laboral

- Auxílio Home Office

- Day Off (Bday)

- Incentivo a saúde mental



Jornada de trabalho: 8h às 18h (2h de intervalo), de segunda a sexta-feira
