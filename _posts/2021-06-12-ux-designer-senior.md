---
title: UX Designer - Sênior
categories:
- job
tag: HTML, CSS, Angular, Front-End
logo: img/empresa/motorola.jpg
layout: post
contratacao: remoto
setor: Desenvolvimento
empresa: Motorola
---

Estamos em busca de um profissional que saiba utilizar as técnicas de UX no planejamento de soluções, garantindo a evolução dos produtos existentes e de novos produtos ou aplicações, com foco no melhor resultado para experiência dos usuários.



A pessoa selecionada ficará responsável por:



- Imersão no negócio do cliente, aplicação de técnicas de Discovery;

- Planejar e conduzir as etapas do projeto junto a times multidisciplinares;

- Executar técnicas de criação, desenvolvimento e implementação de soluções inovadoras e atraentes;

- Realizar processos de ideação, criação de protótipos, interação e testes;

- Realizar pesquisas centradas no usuário e executar testes de usabilidade;

- Documentar processos e resultados dos entregáveis;

- Mapear necessidades, comportamentos e expectativas do usuário;

- Facilitar sessões de cocriação com times multidisciplinares. 

Requisitos
- Formação em Design ou áreas afins (com foco em Design);

- Experiência anterior como UX/UI Designer, com mínimo de 4 anos;

- Experiência em utilização de Design Thinking aplicado a negócios;

- Domínio de ferramentas de prototipação, testes de usabilidade, pesquisa e construções de artefatos;

- Entendimento sobre a criação de mapas de navegação, fluxos e jornadas;

- Habilidade em estruturar e documentar processos;

- Habilidade técnica de negociação com clientes;

- Facilidade em conduzir dinâmicas de cocriação;



Diferencial para a vaga:

- Pós-graduação na área de Design ou Negócios;

- Conhecimento em UX Writing;

- Conhecimento em interfaces modulares, Design Systems e Guias de Estilo;

- Conhecimento em metodologias ágeis.



Benefícios oferecidos: 

- Plano de Saúde e Odontológico;

- Vale Refeição/Alimentação;

- Auxílio Creche;

- Reembolso em certificações;

- Incentivo a educação;

- Seguro de vida

- Ginástica Laboral

- Auxílio Home Office

- Day Off (Bday)

- Incentivo a saúde mental



Jornada de trabalho: 8h às 18h (2h de intervalo), de segunda a sexta-feira.
